var Bicicleta = function(id,color,modelo,ubicacion){
   this.id=id;
   this.color=color;
   this.modelo=modelo;
   this.ubicacion=ubicacion;
}
   Bicicleta.prototype.toString=function(){
       return 'id:'+this.id + " Color:"+this.color;
   }

    Bicicleta.allBicis=[];
    
    Bicicleta.add=function(aBici){
      Bicicleta.allBicis.push(aBici);
   }


  /* var a =new Bicicleta(1,'rojo','urbana',[-33.421787, -70.618638]);
   var b =new Bicicleta(2,'blanca','urbana',[-33.456523, -70.593272]);
   var c =new Bicicleta(3,'verde','urbana',[-33.424928, -70.632895]);
   var d =new Bicicleta(4,'naranja','urbana',[-33.458455, -70.663778]);


   
  
   Bicicleta.add(a);
   Bicicleta.add(b);
   Bicicleta.add(c);
   Bicicleta.add(d);*/


   Bicicleta.findById=function(aBiciId){
     var aBici=Bicicleta.allBicis.find(x=>x.id==aBiciId);
      if(aBici)
         return aBici;
      else
         throw new Error(`No existe bicicleta con el id ${aBiciId}`);

   } 
   
   Bicicleta.removeById=function(aBiciId){
      for(var i=0;i<Bicicleta.allBicis.length;i++){
         
         if(Bicicleta.allBicis[i].id==aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
          }  
      }

   }



   module.exports=Bicicleta;

